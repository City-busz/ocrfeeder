# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-07-31 20:08+0700\n"
"PO-Revision-Date: 2022-12-10 04:53+0700\n"
"Last-Translator: OIS <mistresssilvara@hotmail.com>\n"
"Language-Team: \n"
"Language: ie\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 3.1.1\n"
"X-Poedit-Basepath: ../ocrfeeder-0.8.1/po\n"
"X-Poedit-SearchPath-0: .\n"

#: ../resources/org.gnome.OCRFeeder.desktop.in.h:1
#: ../resources/org.gnome.OCRFeeder.appdata.xml.in.h:1
msgid "OCRFeeder"
msgstr "OCRFeeder"

#: ../resources/org.gnome.OCRFeeder.desktop.in.h:2
msgid "The complete OCR suite."
msgstr "Un paquette de OCR complet."

#: ../resources/org.gnome.OCRFeeder.desktop.in.h:3
msgid ""
"OCR;Optical Character Recognition;CuneiForm;GOCR;Ocrad;Tesseract;Document "
"Recognition;Scanner;Scanning;"
msgstr ""

#: ../resources/org.gnome.OCRFeeder.appdata.xml.in.h:2
msgid "The complete OCR suite"
msgstr "Un complet paquette de OCR"

#: ../resources/org.gnome.OCRFeeder.appdata.xml.in.h:3
msgid ""
"OCRFeeder is a document layout analysis and optical character recognition "
"system."
msgstr ""

#: ../resources/org.gnome.OCRFeeder.appdata.xml.in.h:4
msgid ""
"Given the images it will automatically outline its contents, distinguish "
"between what's graphics and text and perform OCR over the latter. It "
"generates multiple formats being its main one ODT."
msgstr ""

#: ../resources/org.gnome.OCRFeeder.appdata.xml.in.h:5
msgid ""
"It features a complete GTK graphical user interface that allows the users to "
"correct any unrecognized characters, defined or correct bounding boxes, set "
"paragraph styles, clean the input images, import PDFs, save and load the "
"project, export everything to multiple formats, etc."
msgstr ""

#: ../resources/org.gnome.OCRFeeder.appdata.xml.in.h:6
#: ../src/ocrfeeder/studio/widgetPresenter.py:1571
msgid "Content Areas"
msgstr "Areas de contenete"

#: ../resources/org.gnome.OCRFeeder.appdata.xml.in.h:7
#: ../src/ocrfeeder/studio/widgetPresenter.py:924
msgid "Unpaper Image Processor"
msgstr "Processor de images «unpaper»"

#: ../resources/org.gnome.OCRFeeder.appdata.xml.in.h:8
#: ../resources/spell-checker.ui.h:1
msgid "Check spelling"
msgstr "Controlar li ortografie"

#: ../resources/org.gnome.OCRFeeder.appdata.xml.in.h:9
msgid "Bug Fixes:"
msgstr "Correctiones:"

#: ../resources/org.gnome.OCRFeeder.appdata.xml.in.h:10
msgid ""
"Fix PDF importation, which was broken after the Python 3 port (thanks to "
"@scx)"
msgstr ""

#: ../resources/org.gnome.OCRFeeder.appdata.xml.in.h:11
msgid ""
"Allow to import PDF files whose paths have characters Ghostscript doesn't "
"like"
msgstr ""

#: ../resources/org.gnome.OCRFeeder.appdata.xml.in.h:12
msgid "Ship the appdata.xml.in the tarball"
msgstr ""

#: ../resources/org.gnome.OCRFeeder.appdata.xml.in.h:13
msgid "Improvements:"
msgstr "Ameliorationes:"

#: ../resources/org.gnome.OCRFeeder.appdata.xml.in.h:14
msgid "Python 3 is now used, making the unicode issues go away for good"
msgstr ""

#: ../resources/org.gnome.OCRFeeder.appdata.xml.in.h:15
#, fuzzy
msgid "The main window is now bigger"
msgstr "Li menú principal de Nemo es nu celat"

#: ../resources/org.gnome.OCRFeeder.appdata.xml.in.h:16
msgid "The image clip is now expandible"
msgstr ""

#: ../resources/org.gnome.OCRFeeder.appdata.xml.in.h:17
msgid "The manpages have been updated with the missing options"
msgstr ""

#: ../resources/org.gnome.OCRFeeder.appdata.xml.in.h:18
msgid "Fix icons in the boxes' editor text align buttons"
msgstr ""

#: ../resources/org.gnome.OCRFeeder.appdata.xml.in.h:19
msgid "Install the application icon in the right place"
msgstr ""

#: ../resources/org.gnome.OCRFeeder.appdata.xml.in.h:20
msgid ""
"Many issues in the scanner dialog have been fixed (scanner options, UI "
"problems, etc.)"
msgstr ""

#: ../resources/org.gnome.OCRFeeder.appdata.xml.in.h:21
msgid ""
"The Preferences dialog is now smaller (the big contents are wrapped in a "
"scrolled window)"
msgstr ""

#: ../resources/org.gnome.OCRFeeder.appdata.xml.in.h:22
msgid "All dialogs are now centered to the main window"
msgstr ""

#: ../resources/org.gnome.OCRFeeder.appdata.xml.in.h:23
msgid "Use XDG user's configuration directory instead of .ocrfeeder"
msgstr ""

#: ../resources/org.gnome.OCRFeeder.appdata.xml.in.h:24
msgid "Fix Gtk thread related problems"
msgstr ""

#: ../resources/org.gnome.OCRFeeder.appdata.xml.in.h:25
msgid "Fix unicode issues when saving a project and exporting a document"
msgstr ""

#: ../resources/org.gnome.OCRFeeder.appdata.xml.in.h:26
msgid "Fix issue when exporting a picture in a document from a loaded project"
msgstr ""

#: ../resources/org.gnome.OCRFeeder.appdata.xml.in.h:27
msgid "Fix ODT exportation button's icon"
msgstr ""

#: ../resources/org.gnome.OCRFeeder.appdata.xml.in.h:28
msgid "New Features:"
msgstr "Nov caracteristicas:"

#: ../resources/org.gnome.OCRFeeder.appdata.xml.in.h:29
msgid "Add support for multiple image TIFFs"
msgstr ""

#: ../resources/org.gnome.OCRFeeder.appdata.xml.in.h:30
msgid "Port the application to GObject Introspection"
msgstr ""

#: ../resources/org.gnome.OCRFeeder.appdata.xml.in.h:31
msgid "Scan with 300 DPI and in color mode"
msgstr "Scannar con 300 PPI e in li color"

#: ../resources/org.gnome.OCRFeeder.appdata.xml.in.h:32
msgid "Use the last visited directory when adding a new image"
msgstr ""

#: ../resources/org.gnome.OCRFeeder.appdata.xml.in.h:33
msgid ""
"Warn when no OCR engines are found on startup or when performing the "
"recognition"
msgstr ""

#: ../resources/org.gnome.OCRFeeder.appdata.xml.in.h:34
msgid ""
"Update the box editor's OCR controls sensitiveness according to the "
"existence of OCR engines"
msgstr ""

#: ../resources/org.gnome.OCRFeeder.appdata.xml.in.h:35
#, fuzzy
msgid "Fix PIL importation"
msgstr "_Unir ruptet lineas e separat paroles"

#: ../resources/org.gnome.OCRFeeder.appdata.xml.in.h:36
msgid "Fix error when exporting a PDF with empty text areas"
msgstr ""

#: ../resources/org.gnome.OCRFeeder.appdata.xml.in.h:37
msgid "Fix PDF output options in ocrfeeder-cli"
msgstr ""

#: ../resources/org.gnome.OCRFeeder.appdata.xml.in.h:38
msgid "Fix getting engine name in ocrfeeder-cli"
msgstr ""

#: ../resources/org.gnome.OCRFeeder.appdata.xml.in.h:39
msgid "Fix the use of newer versions of Unpaper"
msgstr ""

#: ../resources/org.gnome.OCRFeeder.appdata.xml.in.h:40
msgid "Fix text in the pages icon view"
msgstr ""

#: ../resources/org.gnome.OCRFeeder.appdata.xml.in.h:41
msgid "Fix reordering pages in the icon view"
msgstr ""

#: ../resources/org.gnome.OCRFeeder.appdata.xml.in.h:42
msgid "Fix issues when no locale is set"
msgstr ""

#: ../resources/org.gnome.OCRFeeder.appdata.xml.in.h:43
msgid "Fix loading project with more than one page"
msgstr ""

#: ../resources/org.gnome.OCRFeeder.appdata.xml.in.h:44
msgid "Fix updating the OCR engines in the BoxEditor"
msgstr ""

#: ../resources/org.gnome.OCRFeeder.appdata.xml.in.h:45
msgid "New and Updated Translations:"
msgstr "Nov e actualisat traductiones:"

#: ../resources/org.gnome.OCRFeeder.appdata.xml.in.h:46
msgid "Marek Černocký [cz]"
msgstr ""

#: ../resources/org.gnome.OCRFeeder.appdata.xml.in.h:47
msgid "Daniel Mustieles [es]"
msgstr "Daniel Mustieles [es]"

#: ../resources/org.gnome.OCRFeeder.appdata.xml.in.h:48
msgid "Fran Diéguez [gl]"
msgstr "Fran Diéguez [gl]"

#: ../resources/org.gnome.OCRFeeder.appdata.xml.in.h:49
msgid "Dimitris Spingos [el]"
msgstr ""

#: ../resources/org.gnome.OCRFeeder.appdata.xml.in.h:50
msgid "Aharon Don [he]"
msgstr "Aharon Don [he]"

#: ../resources/org.gnome.OCRFeeder.appdata.xml.in.h:51
msgid "Attila Hammer, Gabor Kelemen [hu]"
msgstr ""

#: ../resources/org.gnome.OCRFeeder.appdata.xml.in.h:52
msgid "Rafael Ferreira [pt_BR]"
msgstr ""

#: ../resources/org.gnome.OCRFeeder.appdata.xml.in.h:53
msgid "Martin Srebotnjak [sl]"
msgstr ""

#: ../resources/org.gnome.OCRFeeder.appdata.xml.in.h:54
msgid "Мирослав Николић [sr]"
msgstr "Мирослав Николић [sr]"

#: ../resources/org.gnome.OCRFeeder.appdata.xml.in.h:55
msgid "Piotr Drąg [uk]"
msgstr "Piotr Drąg [uk]"

#: ../resources/org.gnome.OCRFeeder.appdata.xml.in.h:56
msgid "Wylmer Wang [zh_CN]"
msgstr ""

#: ../resources/spell-checker.ui.h:2
msgid "Misspelled word:"
msgstr "Parol misespelat:"

#: ../resources/spell-checker.ui.h:3
msgid "Change _to:"
msgstr "C_ambiar a:"

#: ../resources/spell-checker.ui.h:4
msgid "_Suggestions:"
msgstr "_Suggestiones:"

#: ../resources/spell-checker.ui.h:5
msgid "Cha_nge"
msgstr "Cam_biar"

#: ../resources/spell-checker.ui.h:6
msgid "Ignore _All"
msgstr "Ignorar _omni"

#: ../resources/spell-checker.ui.h:7
msgid "Change A_ll"
msgstr "Cambiar o_mni"

#: ../resources/spell-checker.ui.h:8
#: ../src/ocrfeeder/studio/studioBuilder.py:422
msgid "_Ignore"
msgstr "_Ignorar"

#: ../resources/spell-checker.ui.h:9
msgid "Language:"
msgstr "Lingue:"

#: ../resources/spell-checker.ui.h:10
msgid "<b>Language</b>"
msgstr "<b>Lingue</b>"

#: ../src/ocrfeeder/feeder/imageManipulation.py:35
#, python-format
msgid ""
"A problem occurred while trying to open the image:\n"
" %s\n"
"Ensure the image exists or try converting it to another format."
msgstr ""

#: ../src/ocrfeeder/studio/studioBuilder.py:62
msgid "ODT"
msgstr "ODT"

#: ../src/ocrfeeder/studio/studioBuilder.py:63
msgid "HTML"
msgstr "HTML"

#: ../src/ocrfeeder/studio/studioBuilder.py:64
#: ../src/ocrfeeder/studio/studioBuilder.py:245
msgid "PDF"
msgstr "PDF"

#: ../src/ocrfeeder/studio/studioBuilder.py:65
msgid "Plain Text"
msgstr "Simplic textu"

#: ../src/ocrfeeder/studio/studioBuilder.py:171
msgid "Images"
msgstr "Images"

#: ../src/ocrfeeder/studio/studioBuilder.py:189
#, fuzzy
msgid "Obtaining scanners"
msgstr "Null scannatores detectet"

#: ../src/ocrfeeder/studio/studioBuilder.py:189
#: ../src/ocrfeeder/studio/studioBuilder.py:215
#: ../src/ocrfeeder/studio/studioBuilder.py:259
#: ../src/ocrfeeder/studio/widgetModeler.py:353
#: ../src/ocrfeeder/studio/widgetModeler.py:358
#: ../src/ocrfeeder/studio/widgetModeler.py:412
#: ../src/ocrfeeder/studio/widgetModeler.py:509
msgid "Please wait…"
msgstr "Ples atender…"

#: ../src/ocrfeeder/studio/studioBuilder.py:215
msgid "Scanning"
msgstr "Scannation"

#: ../src/ocrfeeder/studio/studioBuilder.py:220
#, fuzzy
msgid "No scanner devices were found"
msgstr "Claves esset trovat ma ne importat."

#: ../src/ocrfeeder/studio/studioBuilder.py:221
#: ../src/ocrfeeder/studio/studioBuilder.py:236
#: ../src/ocrfeeder/studio/widgetPresenter.py:1211
#: ../src/ocrfeeder/studio/widgetPresenter.py:1222
msgid "Error"
msgstr "Errore"

#: ../src/ocrfeeder/studio/studioBuilder.py:235
#, fuzzy
msgid "Error scanning page"
msgstr "Un errore evenit cargante págine de auxilie"

#: ../src/ocrfeeder/studio/studioBuilder.py:259
msgid "Loading PDF"
msgstr "Cargante PDF"

#: ../src/ocrfeeder/studio/studioBuilder.py:289
msgid "Export pages"
msgstr "Exportar li págines"

#: ../src/ocrfeeder/studio/studioBuilder.py:308
msgid "Are you sure you want to delete the current image?"
msgstr "Esque vu vole remover li actual image?"

#: ../src/ocrfeeder/studio/studioBuilder.py:380
msgid "Are you sure you want to clear the project?"
msgstr "Esque vu vole vacuar li projecte?"

#: ../src/ocrfeeder/studio/studioBuilder.py:418
#: ../src/ocrfeeder/studio/widgetPresenter.py:1858
msgid "No OCR engines available"
msgstr "Null provisores de OCR disponibil"

#: ../src/ocrfeeder/studio/studioBuilder.py:419
#: ../src/ocrfeeder/studio/widgetPresenter.py:1859
msgid ""
"No OCR engines were found in the system.\n"
"Please make sure you have OCR engines installed and available."
msgstr ""

#: ../src/ocrfeeder/studio/studioBuilder.py:424
#: ../src/ocrfeeder/studio/studioBuilder.py:479
msgid "_Open OCR Engines Manager Dialog"
msgstr ""

#: ../src/ocrfeeder/studio/studioBuilder.py:477
msgid "_Keep Current Configuration"
msgstr "_Retener li actual configuration"

#: ../src/ocrfeeder/studio/studioBuilder.py:481
#, python-format
msgid ""
"The following OCR engines' arguments might need to be updated but it appears "
"you have changed their default configuration so they need to be updated "
"manually:\n"
"  <b>%(engines)s</b>\n"
"\n"
"If you do not want to keep your changes you can just remove the current "
"configuration and have your OCR engines detected again."
msgstr ""

#: ../src/ocrfeeder/studio/studioBuilder.py:502
#, fuzzy
msgid "The project hasn't been saved."
msgstr "Li contenete esset gardat a %s"

#: ../src/ocrfeeder/studio/studioBuilder.py:503
msgid "Do you want to save it before closing?"
msgstr "Esque vu vole gardar it ante que salir?"

#: ../src/ocrfeeder/studio/studioBuilder.py:504
msgid "Close anyway"
msgstr "Cluder s_in egard"

#: ../src/ocrfeeder/studio/widgetModeler.py:69
msgid "Selectable areas"
msgstr "Areas selectabil"

#: ../src/ocrfeeder/studio/widgetModeler.py:353
msgid "Preparing image"
msgstr "Preparante li image"

#: ../src/ocrfeeder/studio/widgetModeler.py:355
#, python-format
msgid "Preparing image %(current_index)s/%(total)s"
msgstr "Preparante image %(current_index)s ex %(total)s"

#: ../src/ocrfeeder/studio/widgetModeler.py:412
#, fuzzy
msgid "Deskewing image"
msgstr "Ne es un image"

#: ../src/ocrfeeder/studio/widgetModeler.py:439
msgid "No images added"
msgstr "Null images adjuntet"

#: ../src/ocrfeeder/studio/widgetModeler.py:442
#, python-format
msgid "Zoom: %s %%"
msgstr "Scale: %s%%"

#: ../src/ocrfeeder/studio/widgetModeler.py:444
#, python-format
msgid "Resolution: %.2f x %.2f"
msgstr "Resolution: %.2f x %.2f"

#: ../src/ocrfeeder/studio/widgetModeler.py:446
#, python-format
msgid "Page size: %i x %i"
msgstr "Dimension de págine: %i x %i"

#: ../src/ocrfeeder/studio/widgetModeler.py:487
msgid ""
"There are changes that may be overwritten by the new recognition.\n"
"\n"
"Do you want to continue?"
msgstr ""

#: ../src/ocrfeeder/studio/widgetModeler.py:509
msgid "Recognizing Page"
msgstr "Reconossente un págine"

#: ../src/ocrfeeder/studio/widgetModeler.py:526
msgid "Recognizing Document"
msgstr "Reconossente li document"

#: ../src/ocrfeeder/studio/widgetModeler.py:527
#, python-format
msgid "Recognizing page %(page_number)s/%(total_pages)s. Please wait…"
msgstr ""

#: ../src/ocrfeeder/studio/widgetModeler.py:601
#, python-format
msgid "Export to %(format_name)s"
msgstr "Exportar a %(format_name)s"

#: ../src/ocrfeeder/studio/widgetModeler.py:625
msgid "What kind of PDF document do you wish?"
msgstr ""

#: ../src/ocrfeeder/studio/widgetModeler.py:627
#, fuzzy
msgid "From scratch"
msgstr "Ex:"

#: ../src/ocrfeeder/studio/widgetModeler.py:629
msgid "Creates a new PDF from scratch."
msgstr ""

#: ../src/ocrfeeder/studio/widgetModeler.py:632
msgid "Searchable PDF"
msgstr "PDF serchabil"

#: ../src/ocrfeeder/studio/widgetModeler.py:633
msgid "Creates a PDF based on the images but with searchable text."
msgstr ""

#: ../src/ocrfeeder/studio/widgetModeler.py:667
msgid "OCRFeeder Projects"
msgstr "Projectes de OCRFeeder"

#: ../src/ocrfeeder/studio/widgetModeler.py:714
#, python-format
msgid ""
"<b>A file named \"%(name)s\" already exists. Do you want to replace it?</b>\n"
"\n"
"The file exists in \"%(dir)s\". Replacing it will overwrite its contents."
msgstr ""

#: ../src/ocrfeeder/studio/widgetModeler.py:721
msgid "Replace"
msgstr "Substituer"

#: ../src/ocrfeeder/studio/widgetPresenter.py:61
#: ../src/ocrfeeder/studio/pagesiconview.py:78
msgid "Pages"
msgstr "Págines"

#: ../src/ocrfeeder/studio/widgetPresenter.py:81
msgid "_File"
msgstr "_File"

#: ../src/ocrfeeder/studio/widgetPresenter.py:82
msgid "_Quit"
msgstr "Sa_lir"

#: ../src/ocrfeeder/studio/widgetPresenter.py:82
msgid "Exit the program"
msgstr "Surtir li programma"

#: ../src/ocrfeeder/studio/widgetPresenter.py:83
msgid "_Open"
msgstr "_Aperter"

#: ../src/ocrfeeder/studio/widgetPresenter.py:83
msgid "Open project"
msgstr "Aperter un projecte"

#: ../src/ocrfeeder/studio/widgetPresenter.py:84
msgid "_Save"
msgstr "_Gardar"

#: ../src/ocrfeeder/studio/widgetPresenter.py:84
msgid "Save project"
msgstr "Gardar li projecte"

#: ../src/ocrfeeder/studio/widgetPresenter.py:85
msgid "_Save As…"
msgstr "Gardar _quam…"

#: ../src/ocrfeeder/studio/widgetPresenter.py:85
msgid "Save project with a chosen name"
msgstr "Gardar li projecte con un selectet nómine"

#: ../src/ocrfeeder/studio/widgetPresenter.py:86
msgid "_Add Image"
msgstr "_Adjunter un image"

#: ../src/ocrfeeder/studio/widgetPresenter.py:86
msgid "Add another image"
msgstr "Adjunter un altri image"

#: ../src/ocrfeeder/studio/widgetPresenter.py:87
msgid "Add _Folder"
msgstr "Adjunter un fólder"

#: ../src/ocrfeeder/studio/widgetPresenter.py:87
msgid "Add all images in a folder"
msgstr "Adjunter omni images in un fólder"

#: ../src/ocrfeeder/studio/widgetPresenter.py:88
msgid "Append Project"
msgstr "Apender un projecte"

#: ../src/ocrfeeder/studio/widgetPresenter.py:88
msgid "Load a project and append it to the current one"
msgstr "Cargar un projecte e apender it al actual"

#: ../src/ocrfeeder/studio/widgetPresenter.py:89
msgid "_Import PDF"
msgstr "_Importar PDF"

#: ../src/ocrfeeder/studio/widgetPresenter.py:89
msgid "Import PDF"
msgstr "Importar PDF"

#: ../src/ocrfeeder/studio/widgetPresenter.py:90
msgid "_Export…"
msgstr "_Exportar…"

#: ../src/ocrfeeder/studio/widgetPresenter.py:90
msgid "Export to a chosen format"
msgstr "Exportar al selectet formate"

#: ../src/ocrfeeder/studio/widgetPresenter.py:91
msgid "_Edit"
msgstr "_Redacter"

#: ../src/ocrfeeder/studio/widgetPresenter.py:92
msgid "_Edit Page"
msgstr "R_edacter li págine"

#: ../src/ocrfeeder/studio/widgetPresenter.py:92
msgid "Edit page settings"
msgstr "Modificar parametres del págine"

#: ../src/ocrfeeder/studio/widgetPresenter.py:93
msgid "_Preferences"
msgstr "_Preferenties"

#: ../src/ocrfeeder/studio/widgetPresenter.py:93
msgid "Configure the application"
msgstr "Configurar li application"

#: ../src/ocrfeeder/studio/widgetPresenter.py:94
msgid "_Delete Page"
msgstr "_Deleter li págine"

#: ../src/ocrfeeder/studio/widgetPresenter.py:94
msgid "Delete current page"
msgstr "Deleter li actual págine"

#: ../src/ocrfeeder/studio/widgetPresenter.py:95
msgid "Move Page Do_wn"
msgstr "Mover li págine a-bass"

#: ../src/ocrfeeder/studio/widgetPresenter.py:95
msgid "Move page down"
msgstr "Mover li págine a-bass"

#: ../src/ocrfeeder/studio/widgetPresenter.py:96
msgid "Move Page Up"
msgstr "Mover li págine ad-up"

#: ../src/ocrfeeder/studio/widgetPresenter.py:96
msgid "Move page up"
msgstr "Mover li págine ad-up"

#: ../src/ocrfeeder/studio/widgetPresenter.py:97
msgid "Select Next Page"
msgstr "Selecter li sequent págine"

#: ../src/ocrfeeder/studio/widgetPresenter.py:97
msgid "Select next page"
msgstr "Selecter li sequent págine"

#: ../src/ocrfeeder/studio/widgetPresenter.py:98
msgid "Select Previous Page"
msgstr "Selecter li precedent págine"

#: ../src/ocrfeeder/studio/widgetPresenter.py:98
msgid "Select previous page"
msgstr "Selecter li precedent págine"

#: ../src/ocrfeeder/studio/widgetPresenter.py:99
msgid "_Clear Project"
msgstr "_Vacuar li projecte"

#: ../src/ocrfeeder/studio/widgetPresenter.py:99
msgid "Delete all images"
msgstr "Deleter omni images"

#: ../src/ocrfeeder/studio/widgetPresenter.py:100
msgid "_View"
msgstr "_Vise"

#: ../src/ocrfeeder/studio/widgetPresenter.py:101
msgid "Zoom In"
msgstr "Agrandar"

#: ../src/ocrfeeder/studio/widgetPresenter.py:102
msgid "Zoom Out"
msgstr "Diminuer"

#: ../src/ocrfeeder/studio/widgetPresenter.py:103
msgid "Best Fit"
msgstr "Adjustar dimension"

#: ../src/ocrfeeder/studio/widgetPresenter.py:104
msgid "Normal Size"
msgstr "Original dimension"

#: ../src/ocrfeeder/studio/widgetPresenter.py:105
msgid "_Document"
msgstr "_Document"

#: ../src/ocrfeeder/studio/widgetPresenter.py:106
#: ../src/ocrfeeder/studio/widgetPresenter.py:1427
msgid "_Tools"
msgstr "_Instrumentarium"

#: ../src/ocrfeeder/studio/widgetPresenter.py:107
msgid "_OCR Engines"
msgstr "Provisores de _OCR"

#: ../src/ocrfeeder/studio/widgetPresenter.py:107
msgid "Manage OCR engines"
msgstr "Gerer li provisores de OCR"

#: ../src/ocrfeeder/studio/widgetPresenter.py:108
msgid "_Unpaper"
msgstr "_Unpaper"

#: ../src/ocrfeeder/studio/widgetPresenter.py:108
#, fuzzy
msgid "Process image with unpaper"
msgstr "Processor de images «unpaper»"

#: ../src/ocrfeeder/studio/widgetPresenter.py:109
#, fuzzy
msgid "Image Des_kewer"
msgstr "Fonde del P_upitre:"

#: ../src/ocrfeeder/studio/widgetPresenter.py:110
#, fuzzy
msgid "Tries to straighten the image"
msgstr "_Rectificar"

#: ../src/ocrfeeder/studio/widgetPresenter.py:112
#: ../src/ocrfeeder/studio/widgetPresenter.py:113
msgid "_Help"
msgstr "Au_xilie"

#: ../src/ocrfeeder/studio/widgetPresenter.py:113
msgid "Help contents"
msgstr "Contenete"

#: ../src/ocrfeeder/studio/widgetPresenter.py:114
msgid "_About"
msgstr "_Pri"

#: ../src/ocrfeeder/studio/widgetPresenter.py:114
msgid "About this application"
msgstr "Pri li application"

#: ../src/ocrfeeder/studio/widgetPresenter.py:116
msgid "_Recognize Document"
msgstr "_Reconosser li document"

#: ../src/ocrfeeder/studio/widgetPresenter.py:117
msgid "Automatically detect and recognize all pages"
msgstr "Automaticmen analisar e reconosser omni págines"

#: ../src/ocrfeeder/studio/widgetPresenter.py:120
msgid "R_ecognize Page"
msgstr "R_econosser li págine"

#: ../src/ocrfeeder/studio/widgetPresenter.py:121
msgid "Automatically detect and recognize the current page"
msgstr "Automaticmen analisar e reconosser li actual págine"

#: ../src/ocrfeeder/studio/widgetPresenter.py:124
msgid "Recognize _Selected Areas"
msgstr "Reconosser li _selectet areas"

#: ../src/ocrfeeder/studio/widgetPresenter.py:125
msgid "Recognize Selected Areas"
msgstr "Reconosser li selectet areas"

#: ../src/ocrfeeder/studio/widgetPresenter.py:128
msgid "Select _All Areas"
msgstr "Selecter _omni areas"

#: ../src/ocrfeeder/studio/widgetPresenter.py:129
msgid "Select all content areas"
msgstr "Reconosser omni areas de contenete"

#: ../src/ocrfeeder/studio/widgetPresenter.py:132
msgid "Select _Previous Area"
msgstr "Selecter li _precedent area"

#: ../src/ocrfeeder/studio/widgetPresenter.py:133
msgid "Select the previous area from the content areas"
msgstr ""

#: ../src/ocrfeeder/studio/widgetPresenter.py:136
msgid "Select _Next Area"
msgstr "Selecter li seque_nt area"

#: ../src/ocrfeeder/studio/widgetPresenter.py:137
msgid "Select the next area from the content areas"
msgstr ""

#: ../src/ocrfeeder/studio/widgetPresenter.py:140
msgid "Delete Selected Areas"
msgstr "Deleter li selectet areas"

#: ../src/ocrfeeder/studio/widgetPresenter.py:141
msgid "Deletes all the currently selected content areas"
msgstr ""

#: ../src/ocrfeeder/studio/widgetPresenter.py:143
msgid "_Generate ODT"
msgstr "Pro_ducter ODT"

#: ../src/ocrfeeder/studio/widgetPresenter.py:143
msgid "Export to ODT"
msgstr "Exportar a ODT"

#: ../src/ocrfeeder/studio/widgetPresenter.py:145
msgid "Import Page From S_canner"
msgstr "Importar un págine del s_cannator"

#: ../src/ocrfeeder/studio/widgetPresenter.py:147
msgid "Import From Scanner"
msgstr "Importar del scannator"

#: ../src/ocrfeeder/studio/widgetPresenter.py:150
msgid "_Copy to Clipboard"
msgstr "_Copiar al Paperiere"

#: ../src/ocrfeeder/studio/widgetPresenter.py:152
msgid "Copy recognized text to clipboard"
msgstr "Copiar li reconosset textu al Paperiere"

#: ../src/ocrfeeder/studio/widgetPresenter.py:155
msgid "Spell_checker"
msgstr "_Controlero de ortografie"

#: ../src/ocrfeeder/studio/widgetPresenter.py:157
msgid "Spell Check Recognized Text"
msgstr "Controlar ortografie del reconosset textu"

#: ../src/ocrfeeder/studio/widgetPresenter.py:261
msgid "No language"
msgstr "Null lingue"

#: ../src/ocrfeeder/studio/widgetPresenter.py:329
msgid "Area editor"
msgstr "Redactor de area"

#: ../src/ocrfeeder/studio/widgetPresenter.py:337
msgid "Sets the content area's upper left corner's X coordinate"
msgstr ""

#: ../src/ocrfeeder/studio/widgetPresenter.py:341
msgid "Sets the content area's upper left corner's Y coordinate"
msgstr ""

#: ../src/ocrfeeder/studio/widgetPresenter.py:345
#, fuzzy
msgid "Sets the content area's width"
msgstr "Provisor de OCR por reconosser ti-ci area de contenete"

#: ../src/ocrfeeder/studio/widgetPresenter.py:348
#, fuzzy
msgid "Sets the content area's height"
msgstr "Provisor de OCR por reconosser ti-ci area de contenete"

#: ../src/ocrfeeder/studio/widgetPresenter.py:351
#: ../src/ocrfeeder/studio/widgetPresenter.py:576
msgid "_Text"
msgstr "_Textu"

#: ../src/ocrfeeder/studio/widgetPresenter.py:352
msgid "Set this content area to be the text type"
msgstr ""

#: ../src/ocrfeeder/studio/widgetPresenter.py:355
msgid "_Image"
msgstr "_Image"

#: ../src/ocrfeeder/studio/widgetPresenter.py:356
msgid "Set this content area to be the image type"
msgstr ""

#: ../src/ocrfeeder/studio/widgetPresenter.py:359
msgid "Type"
msgstr "Tip"

#: ../src/ocrfeeder/studio/widgetPresenter.py:371
msgid "Clip"
msgstr "Tonder"

#: ../src/ocrfeeder/studio/widgetPresenter.py:455
msgid "Bounds"
msgstr "Límites"

#: ../src/ocrfeeder/studio/widgetPresenter.py:460
msgid "_X:"
msgstr "_X:"

#: ../src/ocrfeeder/studio/widgetPresenter.py:469
msgid "_Y:"
msgstr "_Y:"

#: ../src/ocrfeeder/studio/widgetPresenter.py:481
#: ../src/ocrfeeder/studio/widgetPresenter.py:853
msgid "_Width:"
msgstr "_Largore:"

#: ../src/ocrfeeder/studio/widgetPresenter.py:490
msgid "Hei_ght:"
msgstr "Al_tore:"

#: ../src/ocrfeeder/studio/widgetPresenter.py:516
msgid "Left"
msgstr "A levul"

#: ../src/ocrfeeder/studio/widgetPresenter.py:518
#, fuzzy
msgid "Set text to be left aligned"
msgstr "Interspacie del lineas in li textu"

#: ../src/ocrfeeder/studio/widgetPresenter.py:522
msgid "Center"
msgstr "Al centre"

#: ../src/ocrfeeder/studio/widgetPresenter.py:524
#, fuzzy
msgid "Set text to be centered"
msgstr "_Centrat"

#: ../src/ocrfeeder/studio/widgetPresenter.py:528
msgid "Right"
msgstr "A dextri"

#: ../src/ocrfeeder/studio/widgetPresenter.py:530
#, fuzzy
msgid "Set text to be right aligned"
msgstr "Interspacie del lítteres in li textu"

#: ../src/ocrfeeder/studio/widgetPresenter.py:534
msgid "Fill"
msgstr "Justificat"

#: ../src/ocrfeeder/studio/widgetPresenter.py:536
msgid "Set text to be fill its area"
msgstr ""

#: ../src/ocrfeeder/studio/widgetPresenter.py:546
msgid "OC_R"
msgstr "OC_R"

#: ../src/ocrfeeder/studio/widgetPresenter.py:548
msgid "Perform OCR on this content area using the selected OCR engine."
msgstr ""

#: ../src/ocrfeeder/studio/widgetPresenter.py:554
msgid "OCR engine to recognize this content area"
msgstr "Provisor de OCR por reconosser ti-ci area de contenete"

#. Text Properties
#: ../src/ocrfeeder/studio/widgetPresenter.py:561
msgid "Text Properties"
msgstr "Proprietás del textu"

#: ../src/ocrfeeder/studio/widgetPresenter.py:584
msgid "Font"
msgstr "Fonde"

#: ../src/ocrfeeder/studio/widgetPresenter.py:591
msgid "Align"
msgstr "Alignament"

#: ../src/ocrfeeder/studio/widgetPresenter.py:595
msgid "Spacing"
msgstr "Interspacie"

#: ../src/ocrfeeder/studio/widgetPresenter.py:597
msgid "Set the text's letter spacing"
msgstr "Interspacie del lítteres in li textu"

#: ../src/ocrfeeder/studio/widgetPresenter.py:599
msgid "Set the text's line spacing"
msgstr "Interspacie del lineas in li textu"

#: ../src/ocrfeeder/studio/widgetPresenter.py:604
msgid "_Line:"
msgstr "_Linea:"

#: ../src/ocrfeeder/studio/widgetPresenter.py:615
msgid "L_etter:"
msgstr "Lítt_eras:"

#: ../src/ocrfeeder/studio/widgetPresenter.py:627
msgid "Sty_le"
msgstr "Sti_l"

#: ../src/ocrfeeder/studio/widgetPresenter.py:634
msgid "Angle"
msgstr "Angul"

#: ../src/ocrfeeder/studio/widgetPresenter.py:637
msgid "Mis_c"
msgstr "_Varie"

#: ../src/ocrfeeder/studio/widgetPresenter.py:640
#: ../src/ocrfeeder/studio/widgetPresenter.py:1694
msgid "Language"
msgstr "Lingue"

#: ../src/ocrfeeder/studio/widgetPresenter.py:647
msgid "OCR engine to recogni_ze this area:"
msgstr "Pro_visor de OCR por reconosser ti-ci area:"

#: ../src/ocrfeeder/studio/widgetPresenter.py:661
msgid "Detect"
msgstr "_Detecter"

#: ../src/ocrfeeder/studio/widgetPresenter.py:663
msgid "Angle:"
msgstr "Angul:"

#: ../src/ocrfeeder/studio/widgetPresenter.py:750
msgid "Save File"
msgstr "Gardar li file"

#: ../src/ocrfeeder/studio/widgetPresenter.py:753
msgid "Open File"
msgstr "Aperter un file"

#: ../src/ocrfeeder/studio/widgetPresenter.py:757
msgid "Open Folder"
msgstr "Aperter un fólder"

#: ../src/ocrfeeder/studio/widgetPresenter.py:788
msgid "Pages to export"
msgstr "Págines por exportar"

#: ../src/ocrfeeder/studio/widgetPresenter.py:790
msgid "All"
msgstr "Omni"

#: ../src/ocrfeeder/studio/widgetPresenter.py:791
msgid "Current"
msgstr "Actual"

#: ../src/ocrfeeder/studio/widgetPresenter.py:811
msgid "Choose the format"
msgstr "Selecter li formate"

#: ../src/ocrfeeder/studio/widgetPresenter.py:828
#: ../src/ocrfeeder/studio/widgetPresenter.py:839
msgid "Page size"
msgstr "Grandore de págine"

#: ../src/ocrfeeder/studio/widgetPresenter.py:844
#, fuzzy
msgid "Custom…"
msgstr "_Personal"

#: ../src/ocrfeeder/studio/widgetPresenter.py:858
msgid "_Height:"
msgstr "_Altore:"

#: ../src/ocrfeeder/studio/widgetPresenter.py:867
#, fuzzy
msgid "Affected pages"
msgstr "Págines:"

#: ../src/ocrfeeder/studio/widgetPresenter.py:869
msgid "C_urrent"
msgstr "_Actual"

#: ../src/ocrfeeder/studio/widgetPresenter.py:870
msgid "_All"
msgstr "_Omni"

#: ../src/ocrfeeder/studio/widgetPresenter.py:942
msgid "Preview"
msgstr "Previder"

#: ../src/ocrfeeder/studio/widgetPresenter.py:949
msgid "_Preview"
msgstr "_Previder"

#: ../src/ocrfeeder/studio/widgetPresenter.py:976
#, fuzzy
msgid "Performing Unpaper"
msgstr "_Unpaper"

#: ../src/ocrfeeder/studio/widgetPresenter.py:976
#, fuzzy
msgid "Performing unpaper. Please wait…"
msgstr "<b>Ples atender...</b>"

#: ../src/ocrfeeder/studio/widgetPresenter.py:1020
#, fuzzy
msgid "Noise Filter Intensity"
msgstr "Intensitá"

#: ../src/ocrfeeder/studio/widgetPresenter.py:1022
#: ../src/ocrfeeder/studio/widgetPresenter.py:1054
msgid "Default"
msgstr "Predefinit"

#: ../src/ocrfeeder/studio/widgetPresenter.py:1025
#: ../src/ocrfeeder/studio/widgetPresenter.py:1057
msgid "None"
msgstr "Null"

#: ../src/ocrfeeder/studio/widgetPresenter.py:1052
#, fuzzy
msgid "Gray Filter Size"
msgstr "_Gris"

#: ../src/ocrfeeder/studio/widgetPresenter.py:1055
msgid "Custom"
msgstr "Personalisat"

#: ../src/ocrfeeder/studio/widgetPresenter.py:1084
msgid "Black Filter"
msgstr "Filtre de nigri"

#: ../src/ocrfeeder/studio/widgetPresenter.py:1085
msgid "Use"
msgstr "Usar"

#: ../src/ocrfeeder/studio/widgetPresenter.py:1092
msgid "Extra Options"
msgstr "Plu optiones"

#: ../src/ocrfeeder/studio/widgetPresenter.py:1094
#, fuzzy
msgid "Unpaper's command line arguments"
msgstr "Li linea de comandes es tro long"

#: ../src/ocrfeeder/studio/widgetPresenter.py:1151
msgid "Unpaper Preferences"
msgstr "Preferenties de unpaper"

#: ../src/ocrfeeder/studio/widgetPresenter.py:1211
#: ../src/ocrfeeder/studio/widgetPresenter.py:1222
msgid "An error occurred!"
msgstr "Un errore evenit!"

#: ../src/ocrfeeder/studio/widgetPresenter.py:1292
msgid "Cancelled"
msgstr "Annulat"

#: ../src/ocrfeeder/studio/widgetPresenter.py:1308
msgid "Preferences"
msgstr "Preferenties"

#: ../src/ocrfeeder/studio/widgetPresenter.py:1377
msgid "_General"
msgstr "_General"

#: ../src/ocrfeeder/studio/widgetPresenter.py:1385
msgid "_Recognition"
msgstr "_Reconossentie"

#: ../src/ocrfeeder/studio/widgetPresenter.py:1394
msgid "Select boxes' colors"
msgstr "Colores de buxes"

#: ../src/ocrfeeder/studio/widgetPresenter.py:1400
msgid "Te_xt areas' fill color"
msgstr ""

#: ../src/ocrfeeder/studio/widgetPresenter.py:1406
#, fuzzy
msgid "Text areas' _stroke color"
msgstr "Color por areas transparent"

#: ../src/ocrfeeder/studio/widgetPresenter.py:1412
#, fuzzy
msgid "_Image areas' fill color"
msgstr "Color de funde del activ element"

#: ../src/ocrfeeder/studio/widgetPresenter.py:1432
msgid "Path to unpaper"
msgstr "Rute de unpaper"

#: ../src/ocrfeeder/studio/widgetPresenter.py:1436
msgid "Choose"
msgstr "Selecter"

#: ../src/ocrfeeder/studio/widgetPresenter.py:1444
#: ../src/ocrfeeder/studio/widgetPresenter.py:1721
#: ../src/ocrfeeder/studio/widgetPresenter.py:1759
#: ../src/ocrfeeder/studio/widgetPresenter.py:1781
#: ../src/ocrfeeder/studio/widgetPresenter.py:1878
msgid "OCR Engines"
msgstr "Provisores de OCR"

#: ../src/ocrfeeder/studio/widgetPresenter.py:1446
msgid ""
"The engine that should be used when performing the automatic recognition."
msgstr ""

#: ../src/ocrfeeder/studio/widgetPresenter.py:1457
msgid "Favorite _engine:"
msgstr "Pr_eferet provisor:"

#: ../src/ocrfeeder/studio/widgetPresenter.py:1472
msgid "Window size"
msgstr "Dimension del fenestre"

#: ../src/ocrfeeder/studio/widgetPresenter.py:1473
msgid "A_utomatic"
msgstr "A_utomatic"

#: ../src/ocrfeeder/studio/widgetPresenter.py:1474
msgid "Cu_stom"
msgstr "Per_sonalisat"

#: ../src/ocrfeeder/studio/widgetPresenter.py:1484
msgid "The window size is the detection algorithm's subdivision areas' size."
msgstr ""

#: ../src/ocrfeeder/studio/widgetPresenter.py:1497
msgid "Columns Detection"
msgstr "Detection de columnes"

#: ../src/ocrfeeder/studio/widgetPresenter.py:1502
msgid "_Improve columns detection"
msgstr "Amel_iorar li detection de columnes"

#: ../src/ocrfeeder/studio/widgetPresenter.py:1504
msgid "Use a post-detection algorithm to improve the detection of columns"
msgstr ""

#: ../src/ocrfeeder/studio/widgetPresenter.py:1515
#: ../src/ocrfeeder/studio/widgetPresenter.py:1589
msgid "_Automatic"
msgstr "_Automatic"

#: ../src/ocrfeeder/studio/widgetPresenter.py:1516
#: ../src/ocrfeeder/studio/widgetPresenter.py:1590
#, fuzzy
msgid "Custo_m"
msgstr "P_ersonalisat"

#: ../src/ocrfeeder/studio/widgetPresenter.py:1520
msgid "The columns' minimum width in pixels"
msgstr "Li min largore de columnes in pixeles"

#: ../src/ocrfeeder/studio/widgetPresenter.py:1536
msgid "Minimum width that a column should have:"
msgstr ""

#: ../src/ocrfeeder/studio/widgetPresenter.py:1558
msgid "Recognized Text"
msgstr "Reconosset textu"

#: ../src/ocrfeeder/studio/widgetPresenter.py:1559
msgid "_Fix line breaks and hyphenization"
msgstr "_Unir ruptet lineas e separat paroles"

#: ../src/ocrfeeder/studio/widgetPresenter.py:1561
msgid ""
"Removes single line breaks and hyphenization from text generated by OCR "
"engines"
msgstr ""

#: ../src/ocrfeeder/studio/widgetPresenter.py:1576
msgid "A_djust content areas' bounds"
msgstr "A_djustar li límites de areas de contenete"

#: ../src/ocrfeeder/studio/widgetPresenter.py:1578
msgid "Use a post-detection algorithm to shorten the contents areas' margins"
msgstr ""

#: ../src/ocrfeeder/studio/widgetPresenter.py:1594
msgid "The maximum size for the content areas' margins in pixels"
msgstr ""

#: ../src/ocrfeeder/studio/widgetPresenter.py:1611
msgid "Maximum size that the content areas' margins should have:"
msgstr ""

#: ../src/ocrfeeder/studio/widgetPresenter.py:1658
#, fuzzy
msgid "Image Pre-processing"
msgstr "pre"

#: ../src/ocrfeeder/studio/widgetPresenter.py:1659
#, fuzzy
msgid "Des_kew images"
msgstr "Fonde del P_upitre:"

#: ../src/ocrfeeder/studio/widgetPresenter.py:1661
msgid "Tries to straighten the images before they are added"
msgstr ""

#: ../src/ocrfeeder/studio/widgetPresenter.py:1670
#, fuzzy
msgid "_Unpaper images"
msgstr "_Unpaper"

#: ../src/ocrfeeder/studio/widgetPresenter.py:1672
msgid "Cleans the image using the Unpaper pre-processor"
msgstr "Demuddar li image med li preprocessor unpaper"

#: ../src/ocrfeeder/studio/widgetPresenter.py:1676
msgid "Unpaper _Preferences"
msgstr "_Preferenties de unpaper"

#: ../src/ocrfeeder/studio/widgetPresenter.py:1698
msgid ""
"The language may affect how the OCR engines work.\n"
"If an engine is set to support languages but does not support the one "
"chosen, it may result in blank text.\n"
"You can choose \"No Language\" to prevent this."
msgstr ""

#: ../src/ocrfeeder/studio/widgetPresenter.py:1707
msgid "Default _language:"
msgstr "_Lingue predefinit:"

#: ../src/ocrfeeder/studio/widgetPresenter.py:1735
#, fuzzy
msgid "Engines to be added"
msgstr "Provisores de _OCR"

#: ../src/ocrfeeder/studio/widgetPresenter.py:1740
msgid "Include"
msgstr "Includer"

#: ../src/ocrfeeder/studio/widgetPresenter.py:1744
#: ../src/ocrfeeder/studio/widgetPresenter.py:1785
msgid "Engine"
msgstr "Provisor"

#: ../src/ocrfeeder/studio/widgetPresenter.py:1792
msgid "De_tect"
msgstr "De_tecter"

#: ../src/ocrfeeder/studio/widgetPresenter.py:1807
msgid "Are you sure you want to delete this engine?"
msgstr "Esque vu vole remover ti-ci provisor?"

#: ../src/ocrfeeder/studio/widgetPresenter.py:1880
#, python-format
msgid "%s engine"
msgstr "Provisor %s"

#: ../src/ocrfeeder/studio/widgetPresenter.py:1912
msgid "_Name:"
msgstr "_Nómine:"

#: ../src/ocrfeeder/studio/widgetPresenter.py:1913
msgid "Engine name"
msgstr "Nómine del provisor"

#: ../src/ocrfeeder/studio/widgetPresenter.py:1915
msgid "_Image format:"
msgstr "Formate de _image:"

#: ../src/ocrfeeder/studio/widgetPresenter.py:1917
msgid "The required image format"
msgstr "Li besonat formate de image"

#: ../src/ocrfeeder/studio/widgetPresenter.py:1919
#, fuzzy
msgid "_Failure string:"
msgstr "Catene"

#: ../src/ocrfeeder/studio/widgetPresenter.py:1921
msgid "The failure string or character that this engine uses"
msgstr ""

#: ../src/ocrfeeder/studio/widgetPresenter.py:1924
#, fuzzy
msgid "Engine _path:"
msgstr "Rute"

#: ../src/ocrfeeder/studio/widgetPresenter.py:1926
#, fuzzy
msgid "The path to the engine program"
msgstr "Rute"

#: ../src/ocrfeeder/studio/widgetPresenter.py:1928
msgid "Engine _arguments:"
msgstr "_Argumentes de provisor:"

#: ../src/ocrfeeder/studio/widgetPresenter.py:1930
msgid "Arguments: use $IMAGE for image and $FILE if it writes to a file"
msgstr "Argumentes: usa $IMAGE por li image e $FILE si it scri in un file"

#: ../src/ocrfeeder/studio/widgetPresenter.py:1934
msgid "Engine _language argument:"
msgstr "Argumente de _lingue:"

#: ../src/ocrfeeder/studio/widgetPresenter.py:1936
msgid ""
"The language argument in case this engine uses it (for example \"-l\"). In "
"order for it to work, the engine's arguments should have the $LANG keyword."
msgstr ""

#: ../src/ocrfeeder/studio/widgetPresenter.py:1942
msgid "Engine lan_guages:"
msgstr "Lin_gues de provisor:"

#: ../src/ocrfeeder/studio/widgetPresenter.py:1944
msgid ""
"The languages this engine supports. This should be given as pairs of the "
"language in the ISO 639-1 and the engine's corresponding language (for "
"example \"en:eng,pt:por,es:esp\"). In order for it to work, the engine's "
"arguments should have the $LANG keyword."
msgstr ""

#: ../src/ocrfeeder/studio/widgetPresenter.py:1985
msgid "Error setting the new engine; please check your engine settings."
msgstr ""

#: ../src/ocrfeeder/studio/widgetPresenter.py:1985
msgid "Warning"
msgstr "Avise"

#: ../src/ocrfeeder/studio/widgetPresenter.py:2021
msgid "translator-credits"
msgstr "OIS <mistresssilvara@hotmail.com>, 2016-2022"
